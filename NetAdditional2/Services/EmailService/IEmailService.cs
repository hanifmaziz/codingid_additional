﻿using NetAdditional2.Models;

namespace NetAdditional2.Services.EmailService
{
	public interface IEmailService
	{
		void SendEmail(EmailModel request);
	}
}
