﻿namespace NetAdditional2.Models
{
	public class UserModel
	{
		public int UserId { get; set; }
		public string? UserName { get; set; }
		public string? UserEmail { get; set; }
		public string? Password { get; set; }
		public string? UserRole { get; set; }
		public DateTime? LoginTimeStamp { get; set; }
		public bool IsActive { get; set; }
	}
}
