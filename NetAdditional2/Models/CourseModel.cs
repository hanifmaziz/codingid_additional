﻿using Org.BouncyCastle.Asn1.Crmf;

namespace NetAdditional2.Models
{
	public class CourseModel
	{
		public int CourseId { get; set; }
		public int FK_UserId { get; set; }
		public string? CourseName { get; set; }
		public string? CourseDetail { get; set; }
	}
}
