﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using NetAdditional2.Models;
using NetAdditional2.Services.EmailService;
using System.Data;

namespace NetAdditional2.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class CourseController : ControllerBase
	{
		#region Configuration
		private readonly IConfiguration _configuration;
		public CourseController(IConfiguration configuration)
		{
			_configuration = configuration;
		}
		#endregion
		#region Route
		[HttpGet]
		[Authorize(Roles = "Admin,sales")]
		[Route("GetItem")]
		public IActionResult GetItem(string? courseName)
		{
			List<CourseModel> courses = new List<CourseModel>();

			using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
			{


				conn.Open();
				string query = "";
				if (string.IsNullOrEmpty(courseName))
				{
					query = "Select * from tbl_Course";
				}
				else
				{
					query = "Select * from tbl_Course where CourseName = '" + courseName + "'";
				}
				SqlCommand cmd = new SqlCommand(query, conn);
				SqlDataAdapter dtAdapter = new SqlDataAdapter(cmd);
				DataTable dt = new DataTable();
				dtAdapter.Fill(dt);

				foreach (DataRow row in dt.Rows)
				{
					CourseModel courseModel = new CourseModel();
					courseModel.CourseId = Convert.ToInt32(row["CourseId"].ToString());
					courseModel.FK_UserId = Convert.ToInt32(row["FK_UserId"].ToString());
					courseModel.CourseName = row["CourseName"].ToString();
					courseModel.CourseDetail = row["CourseDetail"].ToString();

					courses.Add(courseModel);
				}

				conn.Close();
			}

			return Ok(courses);
		}
		#endregion

		//	private bool ValidateUserRole(UserModel userModel)
		//	{
		//		List<UserModel> users = new List<UserModel>();
		//		using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
		//		{
		//			string query = "select * from tbl_User where UserName = '" + userModel.UserName + "'";
		//			SqlCommand cmd = new SqlCommand(query, conn);
		//			SqlDataAdapter dtAdapter = new SqlDataAdapter(cmd);
		//			DataTable dt = new DataTable();
		//			dtAdapter.Fill(dt);	

		//			foreach(DataRow row in dt.Rows)
		//			{
		//				UserModel user = new UserModel();
		//				user.UserId = Convert.ToInt32(row["UserId"].ToString());
		//				user.UserName = row["UserName"].ToString();
		//				user.UserEmail = row["UserEmail"].ToString();
		//				user.Password = row["Password"].ToString();
		//				user.UserRole = row["UserRole"].ToString();
		//				user.LoginTimeStamp = row["LoginTimeStamp"] == DBNull.Value ? null : Convert.ToDateTime(row["LoginTimeStamp"]);
		//				user.IsActive = Convert.ToBoolean(row["IsActive"].ToString());

		//				users.Add(user);
		//			}
		//		}

		//		var currentUser = users.FirstOrDefault(u => u.UserName.ToLower() == userModel.UserName.ToLower());

		//		if (currentUser != null)
		//		{
		//			return currentUser;
		//		}
		//		return null;
		//	}

	}
}
