﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.IdentityModel.Tokens;
using NetAdditional2.Models;
using NetAdditional2.Services.EmailService;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace NetAdditional2.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class RegisterController : ControllerBase
	{
		#region Configuration
		private readonly IEmailService _emailService;
		private readonly IConfiguration _configuration;
		public RegisterController(IEmailService emailService, IConfiguration configuration)
		{
			_emailService = emailService;
			_configuration = configuration;
		}
		#endregion

		#region Routes
		[HttpPost]
		[Route("user")]
		public IActionResult RegisterUser([FromBody] UserModel user)
		{
			try
			{
				//Insert to database
				using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
				{
					conn.Open();
					string query = "Insert into tbl_User values(@UserName, @UserEmail, @Password, @UserRole, @LoginTimeStamp, @IsActive)";
					SqlCommand cmd = new SqlCommand(query, conn);
					cmd.Parameters.AddWithValue("@UserName", user.UserName == null ? (object)DBNull.Value : user.UserName);
					cmd.Parameters.AddWithValue("@UserEmail", user.UserEmail == null ? (object)DBNull.Value : user.UserEmail);
					cmd.Parameters.AddWithValue("@Password", user.Password == null ? (object)DBNull.Value : user.Password);
					cmd.Parameters.AddWithValue("@UserRole", user.UserRole == null ? (object)DBNull.Value : user.UserRole);
					cmd.Parameters.AddWithValue("@LoginTimeStamp", (object)DBNull.Value);
					cmd.Parameters.AddWithValue("@IsActive", false);
					cmd.ExecuteNonQuery();
					conn.Close();
				}
				//Generate Token to Verify Email
				var token = GenerateToken(user);

				//Sending Email
				EmailModel request = new EmailModel();
				request.To = user.UserEmail;
				request.Subject = "Verify Account";
				request.Body = "<a href=\"http://localhost:22350/api/register/verify?token=[LINK]\">Verify This Link</a>".Replace("[LINK]", token);
				_emailService.SendEmail(request);

				return Ok("User Successfully Added");
			}
			catch (Exception ex)
			{
				return Ok(ex.Message);
			}
		}

		[HttpGet]
		[Route("verify")]
		public IActionResult Verify(string token)
		{
			bool validate = ValidateToken(token);
			if (validate)
			{
				var jwt = new JwtSecurityTokenHandler().ReadJwtToken(token);
				var user = jwt.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
				using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
				{
					conn.Open();
					string query = "Update tbl_User set isActive=1 where UserName = @UserName";
					SqlCommand cmd = new SqlCommand(query, conn);
					cmd.Parameters.AddWithValue("@UserName", user);
					cmd.ExecuteNonQuery();
					conn.Close();

				}
				return Ok("Your Account already active");
			}
			else
			{
				return Ok("You are not authorized");
			}
		}
		#endregion

		#region Private Method
		private string GenerateToken(UserModel user)
		{
			var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
			var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

			var claims = new[]
			{
				new Claim(ClaimTypes.NameIdentifier, user.UserName),
				new Claim(ClaimTypes.Email, user.UserEmail),
				new Claim(ClaimTypes.Role, user.UserRole)
			};

			var token = new JwtSecurityToken(
				_configuration["Jwt:Issuer"],
				_configuration["Jwt:Audience"],
				claims,
				expires: DateTime.Now.AddDays(1),
				signingCredentials: credentials);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}

		private bool ValidateToken(string token)
		{
			var tokenHandler = new JwtSecurityTokenHandler();
			var validationParameters = GetValidationParameters();

			SecurityToken validatedToken;
			try
			{
				IPrincipal principal = tokenHandler.ValidateToken(token, validationParameters, out validatedToken);
				if (principal.Identity.IsAuthenticated)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch 
			{
				return false;
			}
		}

		private TokenValidationParameters GetValidationParameters()
		{
			return new TokenValidationParameters()
			{
				ValidateIssuer = true,
				ValidateAudience = true,
				ValidateLifetime = true,
				ValidIssuer = _configuration["Jwt:Issuer"],
				ValidAudience = _configuration["Jwt:Audience"],
				IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]))
			};
		}
		#endregion
	}
}
