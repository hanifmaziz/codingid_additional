﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.IdentityModel.Tokens;
using NetAdditional2.Models;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security;
using System.Security.Claims;
using System.Text;

namespace NetAdditional2.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class LoginController : ControllerBase
	{
		#region Configuration
		private readonly IConfiguration _configuration;
		public LoginController(IConfiguration configuration)
		{
			_configuration = configuration;
		}
		#endregion

		#region Route
		[HttpPost]
		[Route("user")]
		public IActionResult Login([FromBody] LoginModel userLogin)
		{
			var user = ValidateUser(userLogin);
			if (user != null)
			{
				if (user.IsActive)
				{
					var token = GenerateToken(user);
					return Ok(token);
				}
				return Ok("User is not active");
			}
			return NotFound("User Not Found");
		}
		#endregion

		#region Private Function
		private UserModel ValidateUser(LoginModel userLogin)
		{
			List<UserModel> users = new List<UserModel>();

			using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
			{
				conn.Open();
				string query = "Select * from tbl_User";
				SqlCommand cmd = new SqlCommand(query, conn);
				SqlDataAdapter dtAdapter = new SqlDataAdapter(cmd);
				DataTable dt = new DataTable();
				dtAdapter.Fill(dt);

				foreach (DataRow row in dt.Rows)
				{
					UserModel user = new UserModel();
					user.UserId = Convert.ToInt32(row["UserId"].ToString());
					user.UserName = row["UserName"].ToString();
					user.UserEmail = row["UserEmail"].ToString();
					user.Password = row["Password"].ToString();
					user.UserRole = row["UserRole"].ToString();
					user.LoginTimeStamp = row["LoginTimeStamp"] == DBNull.Value ? null : Convert.ToDateTime(row["LoginTimeStamp"]);
					user.IsActive = Convert.ToBoolean(row["IsActive"].ToString());

					users.Add(user);
				}
				conn.Close();

			}
			var currentUser = users.FirstOrDefault(u => u.UserName.ToLower() == userLogin.UserName && u.Password == userLogin.Password);

			if (currentUser != null)
			{
				return currentUser;
			}
			return null;

		}
		private string GenerateToken(UserModel user)
		{
			var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
			var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

			var claims = new[]
			{
				new Claim(ClaimTypes.NameIdentifier, user.UserName),
				new Claim(ClaimTypes.Email, user.UserEmail),
				new Claim(ClaimTypes.Role, user.UserRole)
			};

			var token = new JwtSecurityToken(
				_configuration["Jwt:Issuer"],
				_configuration["Jwt:Audience"],
				claims,
				expires: DateTime.Now.AddDays(1),
				signingCredentials: credentials);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}
		#endregion
	}
}
